Pupík - pirátský google - proof of concept
========================================================

Co to je: vyhledávání napříč pirátskými zdroji.

Architektura: 
 - jeden kontejner s elasticsearch
 - druhy kontejner "pumps" - datové pumpy v pythonu agregují pirátské zdroje do elasticsearch.
    (V současnosti fungují pumpy na forum, redmine a piroplaceni, ve vyvoji je pumpa na evidenci lobokontaktů.)
 - treti kontejner "frontend" je zalozen na SimpleHTTPServer v pythonu
   Poskytuje na portu 8000 statickou HTML stranku, ve ktere je vytvoren VUE frontend.
   Ten zobrazí vyhledávací pole, provede dotaz via api do ES a zobrazí výsledky.

TODOs: 
zatím jednoduše v souboru TODO.txt

Další rozvoj + poznámky: 
- zprovozněné pumpy a ES úložiště (i bez frontendu) bude sloužit jako backend pro chystaný projekt Alerts
- logstash vs. datové pumpy? https://www.elastic.co/products/logstash


Organizace práce v gitu.
========================================================
Vytvoř si vlastní větev pojmenovanou po sobě. Z ní si vytvářej větve dle své zvyklosti.
Jakmile budeš mít hotovou úpravu, zaintegruj k sobě větev "master" (nikoliv naopak), a pošli merge request.
Integraci do "master" provádí koordinátor projektu, ať v tom není chaos.


Spusteni ostre verze
========================================================
./build.sh 
Sestavi vsechny kontejnery. Pote staci spustit "docker-compose up"


Instalace a spousteni pro dev verzi
========================================================

1) Instalace a spuštění elasticsearch 
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.3.1
pro spuštění použij 
./docker-compose up

pokud nefunguje, pak
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.3.1


(existuje i česká varianta : https://github.com/ludekvesely/elasticsearch-cz , nevyzkoušeno )

2) Pro vývoj si nainstaluj nástroj Kibana (grafický přístup k elasticsearch via browser) https://www.elastic.co/guide/en/kibana/current/docker.html
docker pull docker.elastic.co/kibana/kibana:7.3.1
pro spuštění použij run-kibana.sh
 
pokud nefunguje pak 
docker run --link YOUR_ELASTICSEARCH_CONTAINER_NAME_OR_ID:elasticsearch -p 5601:5601 {docker-repo}:{version}

Kibana pak bezi na http://localhost:5601/

3) Nakonfiguruj index elasticsearch pomocí 
./init-index.sh (!smaže veškerá data!)

4) Pro vývoj frontendu na localhostu otevři soubor frontend/index.txt tímto způsobem (protože jiné způsoby mají problém s cross site reference v javascriptu):
(detaily a troubleshooting k CORS viz https://www.thepolyglotdeveloper.com/2014/08/bypass-cors-errors-testing-apis-locally/ )
- v terminálu naviguj do adresare frontend/
- spust python -m SimpleHTTPServer
- naviguj browser na http://localhost:8000


Datové pumpy
========================================================
jsou v adresáři pumps.

Vývoj:
pres spuštěním nainstaluj virtualenv pomocí install-pumps.sh
samotné pumpy spouštěj ve venv (source venv/bin/activate)
jako samostatné moduly nebo všechny najednou pomocí run.py

Pro společnou aktivaci venv + spuštění run.py je zde soubor run-pumps.sh


Konfigurace a hraní s elastic search
========================================================
pěkný byť mírně zastaralý tutorial na https://www.ludekvesely.cz/serial-elasticsearch-1-zakladni-pojmy/
Vhodný nástroj pro experimenty s nastavením je právě Kibana
Konfigurační příkazy a další tipy jsou v souboru es-config.txt



