#! /usr/bin/python3

""" datova pumpa pro evidenci lobistickych kontaktu.
    Stahuje poslednich 100 schuzek
    API rozhrani na https://evidence-api.pirati.cz/graphql
    format GraphQL https://graphql.org
"""

QUERY = """
{
  searchReports(query: "", first:100) {
    edges {
      node {
        id,
        date,
        title,
        receivedBenefit,
        providedBenefit,
        ourParticipants,
        otherParticipants,
        body,
        author {
          firstName,
          lastName
        }
      }
    }
  }
}

"""

import requests
import base64
import func


def run_query(): 
    request = requests.post('https://evidence-api.pirati.cz/graphql', 
        json={'query': QUERY}, headers={'User-Agent': 'Mozilla'})
    if request.status_code == 200:
        return request.json()
    else:
        print ("Query failed to run by returning code of {}. ".format(request.status_code))


def pump():
    def get_id(transport_id):
        """ Z kodovaneho id vraceneho API evidence dekoduje integer id.
            Reverzni k https://github.com/openlobby/openlobby-app/ .. olapp/core/graphql.py, fce encode_global_id
        """
        tmp = base64.b64decode(transport_id).decode('utf-8')
        return(tmp.split(':')[1]) 

    issues = run_query()["data"]["searchReports"]["edges"]
    
    elastic = list(map(lambda x: {
        "id": get_id(x['node']['id']), 
        "source": "evidence", 
        "url": "https://evidence.pirati.cz/report/%s/" % get_id(x['node']['id']), 
        "date": x['node']['date'], 
        "title": "%s (%s)" % (x['node']['title'], x['node']['ourParticipants']), 
        "snipplet": x["node"]["body"][:200],
        "fulltext": " ".join(["Kontakt/schůzka", x['node']['title'] or "", x['node']['body'] or "", 
            x['node']['ourParticipants'] or "", x['node']['otherParticipants'] or ""]), 
        }, issues))

    func.elastic_save(elastic, 'evidence', True)


if __name__=='__main__':
    pump()
