#! /usr/bin/python3

""" 
Importuje redmine issues do pupiku. Parametry:

    -h  vypise napovedu
    -a  importuje vsechny issues (jinak jen otevrene)
        v takovem pripade ceka 5 sec mezi importem 
        jednotlivych projektu (slusnost vuci API redmine)
        jinak ceka 1 sec
"""    

import time
import func

def arg(argument):
    return func.get_arg(argument, "ha")


def import_redmine_agenda(project, status, import_all):
    """ import_all   .. importuj vsechny issues (jinak jen otevrene) """

    base_url = 'https://redmine.pirati.cz/projects/%s/issues.json?tracker_id=%s' % (project, status)
    if import_all: 
        base_url += '&status_id=*'

    resp = func.get_json(base_url)
    all_issues = []
    if resp:
        original_count = resp['total_count']
        if original_count:
            offset= 0
            while offset < original_count: 
                resp = func.get_json(base_url + '&amp;limit=100&amp;offset=%s' % offset)
                offset +=100
                all_issues.extend(resp['issues'])

    try:
        elastic = list(map(lambda x: {"id": x["id"], 
            "source": "redmine", 
            "url": "https://redmine.pirati.cz/issues/%s" % x["id"], 
            "date": x["created_on"], 
            "title": x["author"]["name"] + " - " + x["subject"], 
            "snipplet": x["description"][:200],
            "fulltext": " ".join([x["author"]["name"], x["subject"], x["description"]])
            }, all_issues))
    except KeyError:    # ignoruj, co s tim taky nadelas. TODO: logovat
        print('KeyError importing %s - %s' % (project, status))
        elastic = []

    print("Redmine %s - %s: importing %s records" % (project, status, len(elastic)))
    func.elastic_save(elastic, 'redmine', True)


def get_projects():
    """ vrat seznam identifikatoru projektu v redmine """
    base_url = 'https://redmine.pirati.cz/projects.json' 
    resp = func.get_json(base_url)
    all_projects = []
    if resp:
        original_count = resp['total_count']
        if original_count:
            offset= 0
            while offset < original_count: 
                resp = func.get_json(base_url + '?limit=100&amp;offset=%s' % offset)
                offset +=100
                all_projects.extend(resp['projects'])

    return list(map(lambda x: x["identifier"], all_projects))


def pump(import_all=False):

    # importuj vsechny projekty ve stavech podani, ukol, schuzka, navrh, dlouhodoby ukol, pro dobrovolniky
    for project in get_projects():
        for status in (12, 2, 13, 14, 15, 20):
            import_redmine_agenda(project, status, import_all)
        time.sleep(5 if import_all else 1) # nepretezovat api


if __name__=='__main__':

    if arg('h'):
        print(__doc__)
        exit()

    pump(arg('a'))
