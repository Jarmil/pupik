#! /usr/bin/python3

""" stahuj posledni prispevky z piratskych listu via RSS feed """

import re
import func
from collections import OrderedDict

def pump():
    feed = func.get_rss('https://www.piratskelisty.cz/rss/')

    entries = None
    for key, value in feed['rss']['channel'].items():
        if key=='item':
            entries = value

    elastic = []
    for entry in entries:

        id = re.findall(r'clanek-([0-9]{1,6})-', entry['guid'])[0]

        elastic.append( {"id": id, 
            "source": "piratskelisty", 
            "url": entry["guid"], 
            "date": entry['pubDate'], 
            "title": entry['title'], 
            "snipplet": entry['description'][:200],
            "fulltext": " ".join([entry['title'], entry['description']])
            })

        print("importing post %s: %s..." % (id, entry['title'][:50]))

    func.elastic_save(elastic, 'piratskelisty', True)


if __name__=='__main__':
    pump()
