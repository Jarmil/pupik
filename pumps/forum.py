#! /usr/bin/python3

""" stahuj posledni prispevky z fora via RSS feed """

import urllib.request
import requests
import re
import xmltodict
import func

def pump():
    feed = func.get_rss('https://forum.pirati.cz/feed')

    elastic = []
    for entry in feed['feed']['entry']:

        id = re.findall(r'#.*$', entry['id'])[0][2:]
        cleantext = re.sub('<[^<]+?>', '', entry['content']['#text'])

        elastic.append( {"id": id, 
            "source": "forum", 
            "url": entry["id"], 
            "date": entry['published'], 
            "title": entry['title']['#text'], 
            "snipplet": cleantext[:200],
            "fulltext": " ".join([entry['title']['#text'], cleantext])
            })

        print("importing post %s: %s..." % (id, entry['title']['#text'][:50]))

    func.elastic_save(elastic, 'forum', True)


if __name__=='__main__':
    pump()
