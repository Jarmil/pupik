#! /usr/bin/python3

""" funkce pro datove pumpy """

import urllib.request
import json
import requests
import os, getopt, sys
import xmltodict


def get_json(url, verbose=False):
    """ Vrat JSON odpoved z adresy URL nebo none v pripade chyby """

    if verbose: print(url)
    req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla'})
    try:
        r = urllib.request.urlopen(req).read()
        return json.loads(r.decode('utf-8'))    
    except Exception as e:
        return None


def get_rss(url):
    """ Vrat RSS feed, formatovany jako OrderedDict """
    req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla'})
    r = urllib.request.urlopen(req).read()
    return xmltodict.parse(r)
    

def elastic_save(items, id_prefix, verbose=False):
    """ vlozi vsechny polozky v poli items do elasticsearch.
        id_prefix .. predpona unikatniho id v ES
        
        pripojuje se na adresu definovanou v promenne prostredi "PUMPS_ES_ADDRESS", neni-li, pak na 'localhost'
    """

    address = os.getenv('PUMPS_ES_ADDRESS', 'localhost')    
    #address = 'vps1.pir-test.eu'

    if verbose: 
        print("Pocet polozek: %s, pripojuji se na %s" % (len(items), address))    

    for item in items:
        url = 'http://%s:9200/article/_doc/%s_%s' % (address, id_prefix, item['id'])
        try:
            r = requests.post(url, json=item)    
        except requests.exceptions.ConnectionError:
            print("ERROR: Cannot connect to ElasticSearch %s" % url)
            return 

        if r.status_code in (403,):
            print("ERROR: ElasticSearch responds 403, disk propably full?")    
        elif not r.status_code in (200, 201):
            print("".join(["Error", str(r.status_code), r.text]))


def get_arg(argumentName, allowedArguments):
    ''' Vraci parametr prikazoveho radku. 
        allowedArguments:	specifikace povolenych argumentu v syntaxi funkce getopt()
        return: 			U parametru typu flag vrati true/false, u retezcoveho aktualni hodnotu.
    '''
    (volbytmp, _) = getopt.getopt(sys.argv[1:],allowedArguments)
    for v in volbytmp: 
        if v[0]=="-" + argumentName: 
            return v[1] if v[1] else True
    return False
