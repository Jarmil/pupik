virtualenv -p python3 venv

source ./venv/bin/activate

pip install requests
pip install xmltodict

# pro code quality
pip install vulture
pip install pyflakes
pip install flake8

