curl -X DELETE "http://localhost:9200/article" -H "Content-Type: application/json"
curl -X PUT "http://localhost:9200/article" -H "Content-Type: application/json" -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 1, 
            "number_of_replicas" : 0 
        },
        "analysis": {
          "analyzer": {
            "czech": {
              "type": "custom",
              "tokenizer": "standard",
              "filter": ["czech_stemmer","asciifolding","lowercase"]
            }
          },
        "filter": {
          "czech_stemmer": {
            "type": "stemmer",
            "name": "czech"
          }
        }          
      }        
    },
    
    "mappings": {
        "properties": {
          "id": {
            "type": "integer"
          },
          "source": {
            "type": "keyword"
          },
          "url": {
            "type": "keyword"
          },
          "date": {
            "type": "text"
          },
          "title": {
            "type": "text"
          },
          "snipplet": {
            "type": "text"
          },
          "fulltext": {
            "type": "text",
            "analyzer": "czech"
          }
        }
    }
}
'
