#! /usr/bin/python3

""" naimportuj vsechny prispevky fora, via web scrapping """

import urllib.request
import requests
import re
import time
import os
import pickle
from bs4 import BeautifulSoup


last_forum_url = None


def elastic_save(items, id_prefix, verbose=False):
    """ vlozi vsechny polozky v poli items do elasticsearch.
        id_prefix .. predpona unikatniho id v ES
    """

    if verbose: 
        print("Pocet polozek: %s" % len(items))    
        
    address = "vps1.pir-test.eu"

    for item in items:
        url = 'http://%s:9200/article/_doc/%s_%s' % (address, id_prefix, item['id'])
        r = requests.post(url, json=item)    
        if not r.status_code in (200, 201):
            print("".join(["Error", str(r.status_code), r.text]))
            raise Exception


def import_page(thread, page):
    """ Importuj jednu stranku vlakna fora """
    global last_forum_url
    
    print("importing %s - %s " % (thread, page))
    
    try:
        req = urllib.request.urlopen("https://forum.pirati.cz/viewtopic.php?t=%s&start=%s" % (thread, page*10))
    except urllib.error.HTTPError as e:
        if e.code==404:
            raise NotImplementedError # pro neexistujici vlakna vraci forum 404
        else:
            print("ERROR", e.code)
            raise Exception
    
    soup = BeautifulSoup(req.read(), 'html.parser')
    posts = soup.find_all("div", class_="postbody")

    # neverejna vlakna nemaji uvedeny atribut
    try:
        base_url = soup.find_all("link",  attrs={"rel": "canonical"} )[0].get('href')
    except IndexError:
        print("UNAUTHORIZED thread %s" % thread)
        with open("unauthorized.txt", "a") as f:
            f.write(str(thread)+ "\n")
        raise NotImplementedError                   

    # protoze forum pro posledni stranku vlakna vraci to same jako pro predposledni
    # porovnej id prvniho prispevku na strance, pokud je shodne jako ulozene, uz se opakujes a skonci
    post_id = posts[0].find_all("h3")[0].find_all("a")[0].get('href')[2:]
    if post_id == last_forum_url:
        raise NotImplementedError 
    else:
        last_forum_url = post_id

    elastic = []
    for post in posts:
    
        # url je v prispevku ulozeno v pitome forme, nutno ponekud prekodovat 
        post_url = post.find_all("a",class_="unread")[0].get('href')
        post_url = re.sub(r'&sid=[0-9abcdef]*#', '#', post_url)
        post_url = re.sub(r'.*php\?p=', 'p=', post_url)
        post_url = base_url + "&amp;" + post_url
        
        post_title = post.find_all("h3")[0].find_all("a")[0].string or ""   # nektere prispevky nemaji titulek
        post_id = post.find_all("h3")[0].find_all("a")[0].get('href')[2:]
        post_text = post.find_all("div", class_="content")[0].get_text()
        post_date = post.find_all("span", class_="responsive-hide")[0].next_sibling.string

        print(post_url) 
        print("    %s" % post_text[:70])
        
        elastic.append( {"id": post_id, 
            "source": "forum", 
            "url": post_url, 
            "date": post_date, 
            "title": post_title, 
            "snipplet": post_text[:200],
            "fulltext": " ".join([post_title, post_text])
            })

    elastic_save(elastic, 'forum', True)        
        

def import_thread(number):
    """ importuj vlakno fora cislo number.
        muze se sestavat z vice podstranek, mezi dotazem na podstranku 
        cekej 1 sec    
    """
    print("Importing thread %s" % number)
    for i in range(0,99999999): # hnus, ale funguje
        try:
            import_page(number, i)
        except NotImplementedError: # dalsi hnus, nechce se mi psat vlastni Exception
            return

        # request na ES sam zabere neco, takze tento nemusi byt cela vterina
        time.sleep(0.5)     


def import_all():
    try:    
        with open("pickle", "rb") as f:
            last = pickle.load(f)
    except:
        last = None
        
    start = last or 1

    for i in range(start+1, 49000):
        import_thread(i)
        last = i
        with open("pickle", "wb") as f:
            pickle.dump(last, f)
        

if __name__=='__main__':

    # obezlicka abych to nemusel nahazovat znova, kdyz spadne sit
    while True:
        try:
            import_all()
        except urllib.error.URLError:
            print("NETWORK ERROR: SLEEPING 5 sec")
            time.sleep(5)
        
       